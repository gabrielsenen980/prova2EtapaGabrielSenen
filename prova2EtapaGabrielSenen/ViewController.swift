//
//  ViewController.swift
//  prova2EtapaGabrielSenen
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica{
    let nomeMusica: String
    let nomeAlbum: String
    let nomeCantor: String
    let nomeImagemPequena: String
    let nomeImagemGrande: String
}


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusica : [Musica] = []
    
    @IBOutlet weak var TableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for:indexPath) as! MyCell
        let musica = self.listaDeMusica[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.nomeImagemPequena)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusica[indice]
        
        detalhesViewController.nomeImagem = musica.nomeImagemGrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
        
        
        
    }
    


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TableView.dataSource = self
        self.TableView.delegate = self
        
        self.listaDeMusica.append(Musica(nomeMusica: "Pontos Crdiais", nomeAlbum: "Album vivo!", nomeCantor: "Alceu Valenca", nomeImagemPequena: "capa_alcau_pequeno", nomeImagemGrande: "capa_alcau_grande" ))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Menos Abandono", nomeAlbum:  "Album Patota de cosmo!", nomeCantor: "zeca pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrande: "capa_zeca_grande" ))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Tiro ao Alvaro", nomeAlbum:  "Album  Adoriam barbosa e convidados!", nomeCantor: "Adoriam barbosa", nomeImagemPequena:"capa_adhoniran_pequeno", nomeImagemGrande: "capa_adhoniran_grande"  ))
        
        TableView.dataSource = self
    }
}

